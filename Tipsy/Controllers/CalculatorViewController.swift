//
//  CalculatorViewController.swift
//  Tipsy
//
//  Created by Leah Joy Ylaya on 12/14/20.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var billTextField: UITextField!
    @IBOutlet weak var zeroPctButton: UIButton!
    @IBOutlet weak var tenPctButton: UIButton!
    @IBOutlet weak var twentyPctButton: UIButton!
    @IBOutlet weak var splitNumberLabel: UILabel!

    lazy var tip: Double = 0.1
    lazy var peopleCount = 2
    lazy var totalBill: Double = 0
    lazy var finalResult = "0.0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.endEditing(_:)))
        view.addGestureRecognizer(tap)
    }

    
    @IBAction func tipChanged(_ sender: UIButton) {
        setSelected(sender)
        let buttonTitle = sender.currentTitle!
        let buttonValue =  String(buttonTitle.dropLast())
        let buttonValueDouble = Double(buttonValue)!
        tip = buttonValueDouble / 100

    }
    
    func setSelected(_ sender: UIButton) {
        zeroPctButton.isSelected = false
        tenPctButton.isSelected = false
        twentyPctButton.isSelected = false
        sender.isSelected = true
        
    }
    
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        splitNumberLabel.text = String(format: "%.0f", sender.value)
        peopleCount = Int(sender.value)
    }
    
    @IBAction func calculatePressed(_ sender: UIButton) {
        if let bill = billTextField.text, !bill.isEmpty {
            totalBill = Double(bill) ?? 0
            let result = totalBill * (1 + tip) / Double(peopleCount)
            finalResult = String(format: "%.2f", result)
        }
        performSegue(withIdentifier: "showResult", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier, identifier == "showResult" {
            let vc = segue.destination as! ResultsViewController
            vc.result = finalResult
            vc.tip = Int(tip * 100)
            vc.split = peopleCount
        }
    }
    
    @objc func endEditing(_ sender: UITapGestureRecognizer? = nil) {
        view.endEditing(true)
    }
}
